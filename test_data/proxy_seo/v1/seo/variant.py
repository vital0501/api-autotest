from test_data.core.variant import BaseStructureData


class StructureData(BaseStructureData):
    structure = {
        "filter[field(:modifier)]": {"name": "filter[field(:modifier)]", "values": [None]},
        "with": {"name": "with", "values": [None]},
        "limit": {"name": "limit", "values": [None]},
        "offset": {"name": "offset", "values": [None]},
    }

    def get_get_data(self):
        return self.get_variants_from_structure()
