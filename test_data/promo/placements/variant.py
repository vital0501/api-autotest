from test_data.core.variant import BaseStructureData


class StructureData(BaseStructureData):
    structure = {
        "limit": {"name": "limit", "values": [None]},
        "offset": {"name": "offset", "values": [None]},
        "filter_alias": {"name": "filter[alias]", "values": [None, 'all-top']},
        "filter_title": {"name": "filter[title]", "values": [None]},
        "filter_shop_id": {"name": "filter[shop_id]", "values": [None, 76378]},
    }

    def get_get_data(self):
        return self.get_variants_from_structure()
