from test_data.core.variant import BaseStructureData


class StructureData(BaseStructureData):
    structure = {
        "placementId": {"name": "placementId", "values": [638]}
    }

    def get_get_data(self):
        return self.get_variants_from_structure()
