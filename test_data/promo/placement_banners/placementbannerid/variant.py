from test_data.core.variant import BaseStructureData


class StructureData(BaseStructureData):
    structure = {
        "placementbannerid": {"name": "placementBannerId", "values": [43]},
    }

    def get_get_data(self):
        return self.get_variants_from_structure()
