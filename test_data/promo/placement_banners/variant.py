from test_data.core.variant import BaseStructureData


class StructureData(BaseStructureData):
    structure = {
        "limit": {"name": "limit", "values": [None]},
        "offset": {"name": "offset", "values": [None]},
        "filter_banner_id": {"name": "filter[banner_id]", "values": [None, 43]},
        "filter_placement_id": {"name": "filter[placement_id]", "values": [None, 1]},
    }

    def get_get_data(self):
        return self.get_variants_from_structure()
