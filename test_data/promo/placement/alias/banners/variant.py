from test_data.core.variant import BaseStructureData


class StructureData(BaseStructureData):
    structure = {
        "alias": {"name": "alias", "values": ['all-top']}
    }

    def get_get_data(self):
        return self.get_variants_from_structure()
