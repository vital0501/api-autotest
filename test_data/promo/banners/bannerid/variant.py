from test_data.core.variant import BaseStructureData


class StructureData(BaseStructureData):
    structure = {
        "bannerId": {"name": "bannerId", "values": [3]},
    }

    def get_get_data(self):
        return self.get_variants_from_structure()
