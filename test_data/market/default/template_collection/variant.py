from test_data.core.variant import BaseStructureData


class StructureData(BaseStructureData):
    structure = {
        "is_active": {"name": "is_active", "values": [None]},
        "inherit": {"name": "inherit", "values": [None]},
        "url_alias": {"name": "url_alias", "values": [None]},
        "parent_template_collection_id": {"name": "parent_template_collection_id", "values": [None]}
    }

    def get_get_data(self):
        return self.get_variants_from_structure()
