from test_data.core.variant import BaseStructureData


class StructureData(BaseStructureData):
    structure = {
        "orderid": {"name": "orderId", "values": [21810000]},
        "email": {"name": "email", "values": ['autotest@vsemayki.ru']}
    }

    def get_get_data(self):
        return self.get_variants_from_structure()
