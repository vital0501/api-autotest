from test_data.core.variant import BaseStructureData


class StructureData(BaseStructureData):

    def get_post_data(self):
        return [
            {
                "cart": [
                    {
                        "design": 1067272,
                        "product_type": "man_hoodie_jacket",
                        "count": 1,
                        "size": "s_44-46",
                        "color": "white",
                        "type": "catalog",
                        "price": 2290,
                        "old_price": 2795
                    }
                ],
                "receiver": {
                    "name": "testApi",
                    "phone": "79000000000",
                    "email": "autotest@vsemayki.ru",
                    "address": {
                        "city": "Новосибирск",
                        "address": "Тестовая"
                    }
                }
            }
        ]
