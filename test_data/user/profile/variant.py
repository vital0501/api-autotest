from test_data.core.variant import BaseStructureData


class StructureData(BaseStructureData):
    user_auth = True

    def get_get_data(self):
        return self.get_variants_from_structure()

    def get_put_data(self):
        return [
            {"userData": {
                "id": 3190132,
                "full_name": "TestName for Autotest",
                "email": "autotest@vsemayki.ru"
            }
            }
        ]

