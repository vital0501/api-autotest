from test_data.core.variant import BaseStructureData


class StructureData(BaseStructureData):
    user_auth = True
    structure = {
        "orderid": {"name": "orderId", "values": [21810000]},
    }

    def get_get_data(self):
        return self.get_variants_from_structure()
