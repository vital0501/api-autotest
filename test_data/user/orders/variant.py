from test_data.core.variant import BaseStructureData


class StructureData(BaseStructureData):
    user_auth = True
    structure = {
        "limit": {"name": "limit", "values": [None]},
        "offset": {"name": "offset", "values": [None]}
    }

    def get_get_data(self):
        return self.get_variants_from_structure()

    @staticmethod
    def get_collection_json_path():
        return 'orders'
