from test_data.core.variant import BaseStructureData


class StructureData(BaseStructureData):
    user_auth = True

    def get_post_data(self):
        return [
            {
                "client_id": "vsemayki_ru",
                "client_secret": "uDLPvBH3o8mtWD3mgPKQLdmHnU7xsWWz",
                "grant_type": "password",
                "username": "autotest@vsemayki.ru",
                "password": "qweasdzxc"
            }
        ]
