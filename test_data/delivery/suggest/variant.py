from test_data.core.variant import BaseStructureData


class StructureData(BaseStructureData):
    structure = {
        "text": {"name": "text", "values": ['новоси']}
    }

    def get_get_data(self):
        return self.get_variants_from_structure()
