from test_data.core.variant import BaseStructureData


class StructureData(BaseStructureData):
    structure = {
        "city": {"name": "city", "values": [None, "Новосибирск"]},
        "deliveryAlias": {"name": "deliveryAlias", "values": [None]}
    }

    def get_get_data(self):
        return self.get_variants_from_structure()

    def get_head_data(self):
        return [None]
