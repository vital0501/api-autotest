import itertools


class BaseStructureData(object):
    structure = {}
    auth = True
    user_auth = False

    @staticmethod
    def get_collection_json_path():
        return None

    def get_structure_data(self, method):
        return self.get_data(method=method)

    def get_data(self, method):
        data_method_name = 'get_{0}_data'.format(method.lower())
        return getattr(self, data_method_name)()

    def get_required_data(self, method):
        data_method_name = 'get_required_{0}_data'.format(method.lower())
        return getattr(self, data_method_name)()

    def get_required_get_data(self):
        return [None]

    @staticmethod
    def get_required_head_data():
        return []

    @staticmethod
    def get_required_post_data():
        return []

    @staticmethod
    def get_required_put_data():
        return []

    @staticmethod
    def get_required_delete_data():
        return []

    def get_get_data(self):
        return []

    def get_head_data(self):
        return []

    def get_post_data(self):
        return []

    def get_put_data(self):
        return []

    @staticmethod
    def get_delete_data():
        return []

    @staticmethod
    def get_variants(*args):
        _param_collection = []

        for item in args:
            _tmp = []
            name = item['name']
            for value in item['values']:
                _tmp.append({name: value})
            _param_collection.append(_tmp)

        for item in list(itertools.product(*_param_collection)):
            _tmp = {}
            for _filter in item:
                for _key in _filter:
                    _tmp.update({_key: _filter[_key]})
            yield _tmp

    def get_variants_from_structure(self):
        return self.get_variants(*[self.structure[item] for item in self.structure.keys()])


