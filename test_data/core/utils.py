import importlib
import json
import platform

from run import TEST_ENC


def get_all_requests(path, has_parameter=None):
    data = json.loads(open(path, 'r', encoding=TEST_ENC).read())
    _data_path = sorted(data['paths'])

    result = []
    for path in _data_path:
        _data_method = sorted(data['paths'][path])
        for method in _data_method:
            if has_parameter is not None:
                if 'parameters' in data['paths'][path][method]:
                    for _parameter_data in data['paths'][path][method]['parameters']:
                        if 'name' in _parameter_data.keys():
                            if has_parameter == _parameter_data['name']:
                                result.append((method, path))
            else:
                result.append((method, path))
    return result


def get_structure_object(path, raise_error=False):
    import_str = 'test_data.' + '.'.join([item for item in path.replace('-', '_').split('/') if item]) + '.variant'
    if '{' in import_str:
        item_by_find = import_str[import_str.find('{') + 1:import_str.find('}')]
        import_str = import_str.replace("{" + item_by_find + "}", item_by_find).lower()

    try:
        return getattr(importlib.import_module(import_str), 'StructureData')
    except (ImportError, AttributeError) as e:
        if raise_error:
            raise e
        else:
            return None


def path_normalize(path, value_item):
    item_by_find = get_query_id_parameter(path=path)
    if item_by_find:
        path = path.replace(item_by_find, '0').format(value_item[item_by_find])
    return path


def get_query_id_parameter(path):
    if '{' in path:
        return path[path.find('{') + 1:path.find('}')]
    else:
        return None


def get_values(structure_obj, method, data_filter='structure_data', raise_error=False):
    obj = structure_obj()
    if hasattr(obj, data_filter):
        values = getattr(structure_obj(), data_filter)(method=method)
    elif hasattr(obj, 'get_{0}'.format(data_filter)):
        values = getattr(structure_obj(), 'get_{0}'.format(data_filter))(method=method)
    else:
        if raise_error:
            raise AttributeError('Data structure for {0} not found!'.format(obj.__class__))
        else:
            values = {}
    return values


def get_description(method, path, file_path, encoding="UTF8"):
    data = json.loads(open(file_path, 'r', encoding=encoding).read())
    _data_path = data['paths']

    for _path in _data_path:
        if _path == path:
            for _method in _data_path[_path]:
                if _method == method:
                    return _data_path[_path][_method]['summary']
