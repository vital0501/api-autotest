import datetime

from test_data.core.variant import BaseStructureData


class StructureData(BaseStructureData):
    structure = {
        "id": {"name": "id", "values": [137]}
    }

    def get_get_data(self):
        return self.get_variants_from_structure()

    def get_put_data(self):
        return [
            {
                "id": 2278,
                "title": "ApiTestMenuChange {0}".format(datetime.datetime.now()),
                "items": "[{\"id\": \"test\"}]"
            },
        ]
