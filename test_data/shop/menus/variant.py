import datetime

from test_data.core.variant import BaseStructureData


class StructureData(BaseStructureData):
    structure = {
        "offset": {"name": "offset", "values": [None]},
        "limit": {"name": "limit", "values": [None]},
        "filter_id": {"name": "filter[id][]", "values": [None, 132]}
    }

    def get_get_data(self):
        return self.get_variants_from_structure()

    def get_post_data(self):
        return [
            {
                "title": "ApiTestMenu {0}".format(datetime.datetime.now()),
                "items": "[]"
            },
            {
                "title": "ApiTestMenu {0}".format(datetime.datetime.now()),
                "items": "[{\"id\": \"test\"}]"
            }
        ]
