from test_data.core.variant import BaseStructureData


class StructureData(BaseStructureData):
    structure = {}

    def get_post_data(self):
        return [{"userData": [{"id": 21810000, "email": "autotest@vsemayki.ru"},
                              {"id": 21795853, "email": "autotest_lk@vsemayki.ru"}]}]
