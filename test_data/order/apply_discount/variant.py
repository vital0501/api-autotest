from test_data.core.variant import BaseStructureData


class StructureData(BaseStructureData):
    structure = {}

    def get_post_data(self):
        return [MIN_VALUE]


MIN_VALUE = {
    "cart": [
        {
            "article": 1305241,
            "product_tkey": "manshortfull",
            "color_tkey": "white",
            "size_tkey": "xs_42-44",
            "qty": 1,
            "price": 1290
        }
    ],
    "promocode": "autotest10"
}
