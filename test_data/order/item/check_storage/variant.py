from test_data.core.variant import BaseStructureData


class StructureData(BaseStructureData):
    structure = {}

    def get_post_data(self):
        return [FULL_VALUE]


FULL_VALUE = {
    "cart":
        [
            {
                "product_type": "manshortfull",
                "size": "xs_42-44",
                "color": "white",
                "type": "constructor",
                "count": 1,
                "design": 990022
            }
        ]
}
