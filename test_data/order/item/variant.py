from test_data.core.variant import BaseStructureData


class StructureData(BaseStructureData):

    def get_post_data(self):
        return [
            {
                "product_type": "man_hoodie_jacket",
                "size": "s_44-46",
                "color": "white",
                "prints": [
                    {
                        "side": "front",
                        "data": "test",
                        "position": {
                            "x": 0,
                            "y": 0
                        },
                        "size": {
                            "width": 0,
                            "height": 0
                        }
                    }
                ]
            }
        ]
