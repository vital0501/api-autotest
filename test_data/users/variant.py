import datetime
import random

from test_data.core.variant import BaseStructureData
from test_data.oauth2.token.variant import User


class StructureData(BaseStructureData):
    auth = False

    def get_post_data(self):
        user = User()
        random_email = "test_{0}_{1}@test.ru".format(
            str(datetime.datetime.now())
                .replace(' ', "_")
                .replace('.', "_")
                .replace(':', "_")
                .replace('-', "_"),
            random.randint(0, 10000))

        return [
            {
                "grant_type": "password",
                "client_id": user.client_id,
                "client_secret": user.client_secret,
                "email": random_email,
                "password": "testpass",
                "phone": "79010000000"
            }
        ]
