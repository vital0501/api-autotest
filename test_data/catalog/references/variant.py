from test_data.core.variant import BaseStructureData


class StructureData(BaseStructureData):
    structure = {
        "output": {"name": "output", "values": [None, 'associative', 'array']},
    }

    def get_get_data(self):
        return self.get_variants(self.structure['output'])

    def get_head_data(self):
        return [None]
