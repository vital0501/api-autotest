from test_data.core.variant import BaseStructureData


class StructureData(BaseStructureData):
    structure = {
        "filter": {"name": "filter", "values": [None, 'constructor', 'sport_constructor']},
        "newCatalog": {"name": "newCatalog", "values": [1, ]}
    }

    def get_get_data(self):
        return self.get_variants(self.structure['filter'], self.structure['newCatalog'])

    def get_head_data(self):
        return [None]

