from test_data.core.variant import BaseStructureData


class StructureData(BaseStructureData):
    structure = {
        "template": {"name": "template", "values": [1305241]}
    }

    def get_get_data(self):
        return self.get_variants_from_structure()
