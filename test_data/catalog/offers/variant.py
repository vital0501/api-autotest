from test_data.core.variant import BaseStructureData


class StructureData(BaseStructureData):

    structure = {
        "offset": {"name": "offset", "values": [None]},
        "limit": {"name": "limit", "values": [None]},
        "search": {"name": "filter[search]", "values": [None, 1305241]},
        "template": {"name": "filter[template]]", "values": [None]},
        "product": {"name": "filter[product]", "values": [None]},
        "collection": {"name": "filter[collection]", "values": [None]},
        "sex": {"name": "filter[sex]", "values": ['male', 'female', None]},
        "sort": {"name": "sort", "values": ['new', 'sell', None]},
    }

    def get_get_data(self):
        return self.get_variants_from_structure()

    @staticmethod
    def get_collection_json_path():
        return 'items'
