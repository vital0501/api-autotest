from test_data.core.variant import BaseStructureData


class StructureData(BaseStructureData):
    structure = {
        "itemId": {"name": "itemId", "values": ['1305241_child_leggings']},
        "ignore_store": {"name": "ignore_store", "values": [None, 'true', 'false']}
    }

    def get_get_data(self):
        return self.get_variants_from_structure()
