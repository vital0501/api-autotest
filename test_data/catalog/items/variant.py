from test_data.core.variant import BaseStructureData


class StructureData(BaseStructureData):
    structure = {
        "offset": {"name": "offset", "values": [None]},
        "limit": {"name": "limit", "values": [None]},
        "category": {"name": "filter[category]", "values": ['child_leggings', None]},
        "tag": {"name": "filter[tag]", "values": [None]},
        "search": {"name": "filter[search]", "values": [None, 1305241]},
        "sex": {"name": "filter[sex]", "values": ['male', 'female', None]},
        "nickname": {"name": "filter[nickname]", "values": [None, 'Кавардак']},
        "with": {"name": "filter[with]", "values": ["tags", "keywords", "categories", "variants", "design", None]},
        "sort": {"name": "sort", "values": ['new', 'sell', None]},
    }

    variant_sort = [{"sort": None}, {"sort": 'new'}, {"sort": 'sell'}]

    def get_get_data(self):
        return self.get_variants_from_structure()

    def get_head_data(self):
        return [None]

    @staticmethod
    def get_collection_json_path():
        return 'items'
