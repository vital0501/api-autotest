from test_data.core.variant import BaseStructureData


class StructureData(BaseStructureData):
    structure = {'query': {'values': ['май'], 'name': "query"}}

    def get_get_data(self):
        return self.get_variants(self.structure['query'])

    def get_required_get_data(self):
        return self.get_variants(self.structure['query'])

    @staticmethod
    def get_collection_json_path():
        return 'data.0.items'
