from test_data.core.variant import BaseStructureData


class StructureData(BaseStructureData):
    structure = {
        "id": {"name": "id", "values": ['familylook']}
    }

    def get_get_data(self):
        return self.get_variants_from_structure()