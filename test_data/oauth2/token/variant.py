from test_data.core.variant import BaseStructureData


class User(object):

    client_id = "vsemayki_ru"
    client_secret = "uDLPvBH3o8mtWD3mgPKQLdmHnU7xsWWz"
    grant_type = "password"
    username = "autotest@vsemayki.ru"
    password = "qweasdzxc"


class UserNoCancelOrder(object):
    client_id = "vsemayki_ru"
    client_secret = "uDLPvBH3o8mtWD3mgPKQLdmHnU7xsWWz"
    grant_type = "password"
    username = "autotest_lk@vsemayki.ru"
    password = "qweasdzxc"


class VMR(object):
    client_id = "vsemayki_ru"
    client_secret = "uDLPvBH3o8mtWD3mgPKQLdmHnU7xsWWz"
    grant_type = "client_credentials"
    username = None
    password = None


class StructureData(BaseStructureData):
    auth = False

    post_data = [
        {"client_id": "vsemayki_ru",
         "client_secret": "uDLPvBH3o8mtWD3mgPKQLdmHnU7xsWWz",
         "grant_type": "client_credentials",
         "username": None,
         "password": None},
        {"client_id": "vsemayki_ru",
         "client_secret": "uDLPvBH3o8mtWD3mgPKQLdmHnU7xsWWz",
         "grant_type": "password",
         "username": 'autotest@vsemayki.ru',
         "password": 'qweasdzxc'}
    ]

    def get_post_data(self):
        return self.post_data
