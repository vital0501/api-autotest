import pytest


def pytest_addoption(parser):
    parser.addoption("--test_run_id", action="store", help="TestRunId")
    parser.addoption("--api_url", action="store", help="Api Url Base")


@pytest.fixture(scope='class')
def test_run_id(request):
    return request.config.getoption("--test_run_id")


@pytest.fixture(scope='class')
def api_url(request):
    return request.config.getoption("--api_url")
