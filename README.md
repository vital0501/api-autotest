###Деплой веток
Для деплоя кастомных веток существует [таска на дженкинсе](http://jenkins.vsemayki.pro/view/Autotest%20deploy/job/autotests_api_dev_deploy_branch/)

В параметрах указываем имя ветки. Деплой происходит на сервер автотестов по пути:

```commandline
/home/vsemayki/projects/autotests_api_dev/%name%
``` 

###Запуск тестов
```commandline
cd /home/vsemayki/projects/autotests_api_dev/%branch_name%
python3.6 run.py --task_id ${JOB_NAME}-${BUILD_NUMBER} --config %config_file_name%
```

Где:

--task_id ${JOB_NAME}-${BUILD_NUMBER} - Параметр для запуска под Дженкинсом. Нужен для генерации отчетов

--config - Имя конфигурационного файла. По умолчанию "default"

ДОП: 

Конфигурационные файлы требуется хранить в папке config проекта


###Структура конфигурационного файла

```json

{
  "api_url": "",
  "swagger_url": "",
  "allure_log": true,
  "allure_dir": "",
  "allure_html": "",
  "flow": 0,
  "debug": false,
  "structure": true,
  "functional": true,
  "coating": true
}
```

api_url - Ссылка на тестируемый инстанс

swagger_url - Ссылка на датаконтракт

allure_log - Требуется ли логирование в Allure

allure_dir - Путь для хранения xml отчетов Allure

allure_html - Путь для хранения сгенерированных отчетов Allure

flow - Количество параллельных потоков для тестов

debug - Включить режим отладки. В этом случае запускаются только тесты с меткой "debug" (pytest.mark.debug чтобы 
отметить тест)

structure - Запускать тесты структуры ответов

functional - Запускать функциональные тесты

coating - Запускать тесты покрытия


ДОП:
 
При structure=false никакие другие тесты не запускаются.