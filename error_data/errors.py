import json


class ApiFindError(object):
    def __init__(self):
        self._data = None
        self.__load_error()

    def find_error(self, request_id):
        if request_id in self._data:
            return self._data[request_id]

    def __load_error(self):
        self._data = self.__load_data()

    @staticmethod
    def __load_data():
        return json.loads(open('errors.json', 'r', encoding='UTF8').read())
