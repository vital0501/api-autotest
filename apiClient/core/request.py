import requests

from flex.core import validate_api_call
from core.exceptions import StructureApiException


class Response(requests.Response):
    _response = None
    _sw_id = None
    _schema = None

    def get_r_type(self):
        return self._sw_id.split("|")[1]

    def get_sw_name(self):
        return self._sw_id.split("|")[0]

    def __init__(self):
        super().__init__()

    def get_sw_id(self):
        return self._sw_id

    @classmethod
    def from_requests_response(cls, response, schema, sw_id=None):
        _cls = cls()
        _cls._response = response
        _cls._sw_id = sw_id
        _cls._schema = schema
        return _cls

    def __getattribute__(self, item):
        if hasattr(object.__getattribute__(self, '_response'), item):
            return getattr(object.__getattribute__(self, '_response'), item)
        else:
            return object.__getattribute__(self, item)

    def check_structure(self):
        validate_api_call(self._schema, raw_request=self.request, raw_response=self)
        self.check_response_code()

    def check_response_code(self):
        if self.status_code not in [200, 201]:
            raise StructureApiException("Ошибка при выполнении запроса. Возвращен код {0}" .format(self.status_code))
