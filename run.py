import datetime
import json
import platform
from optparse import OptionParser

import os

import pytest
import shutil

import requests

__version__ = 0.1

BASE_DIR = str(os.path.normpath(os.path.dirname(os.path.realpath(__file__))))
TEST_DIR = str(os.path.join(BASE_DIR, 'test'))

if platform.system() == 'Windows':
    TEST_ENC = 'CP1251'
else:
    TEST_ENC = 'UTF8'


if platform.system() == 'Windows':
    ALLURE_BIN_PATH = os.path.join(BASE_DIR, "exclude", "allure", "bin", "allure.bat")
else:
    ALLURE_BIN_PATH = os.path.join(BASE_DIR, "exclude", "allure", "bin", "allure")


def generate_run_command(allure_log, allure_dir, flow, test_run_id, api_url, debug, test_name=None):
    log = '-v'
    command = [
        '--test_run_id={0}'.format(test_run_id),
        '--api_url={0}'.format(api_url),
    ]

    if debug:
        command.append('-m debug')

    if test_name:
        command.append(os.path.join(TEST_DIR, '{0}.py'.format(test_name)))
        if test_name in ['test_prepare', 'test_coating']:
            flow = 1
    else:
        for file in os.listdir(TEST_DIR):
            _file_path = os.path.join(TEST_DIR, file)
            if file not in ['test_coating.py', 'test_structure.py']:
                if os.path.isfile(_file_path) and file.startswith('test_'):
                    command.append(_file_path)

    command.append(log)

    if allure_log:
        command.append('--alluredir={0}'.format(allure_dir))

    if int(flow) > 1:
        command.append('-n{0}'.format(flow))

    return command


def get_config_file_name(config):
    return config if config.endswith('.json') else config + '.json'


def main():
    parser = OptionParser(version=__version__, description='Run autotest')

    # Обязательные параметры

    # Используемый config файл
    parser.add_option('--config',
                      action='store',
                      dest='config',
                      help='config_name',
                      default='default.json')

    # Необязательные параметры

    # Параметр при запуске из Jenkins вида %Имя таски%-%Номер билда%
    parser.add_option('--task_id',
                      action='store',
                      dest='task_id',
                      help='Set task_id',
                      default=None)

    # Считываем параметры командной строки
    (options, args) = parser.parse_args()

    # Генерируем TestRun ID
    _test_run_id = str(datetime.datetime.now().timestamp()).replace('.', '')

    # Загружаем настройки
    config_name = get_config_file_name(options.config)
    settings_data = json.loads(open(os.path.join('config', config_name), 'r', encoding='UTF8').read())
    print(settings_data)

    # Подготавливаем пути для отчетов Allure
    allure_dir = settings_data['allure_dir']
    allure_html = settings_data['allure_html']

    if options.task_id:
        allure_dir = os.path.join(settings_data['allure_dir'], _test_run_id)
    else:
        if os.path.exists(allure_dir):
            shutil.rmtree(allure_dir)
        if os.path.exists(allure_html):
            shutil.rmtree(allure_html)

    # Скачиваем api.json
    r = requests.get(settings_data['swagger_url'])
    with open(os.path.normpath(os.path.join(BASE_DIR, 'tmp', "test_api.json")), "w", encoding=TEST_ENC) as code:
        code.writelines(r.content.decode('UTF8'))

    # Запускаем подготовочные тесты. Определяем access token для последующих тестов
    pytest.main(generate_run_command(allure_log=settings_data['allure_log'],
                                     allure_dir=allure_dir,
                                     flow=settings_data['flow'],
                                     api_url=settings_data['api_url'],
                                     test_run_id=_test_run_id,
                                     debug=settings_data['debug'],
                                     test_name="test_prepare"))

    # Запускаем тесты структуры
    if settings_data['structure']:
        pytest.main(generate_run_command(allure_log=settings_data['allure_log'],
                                         allure_dir=allure_dir,
                                         flow=settings_data['flow'],
                                         api_url=settings_data['api_url'],
                                         test_run_id=_test_run_id,
                                         debug=settings_data['debug'],
                                         test_name='test_structure'))

    # Запускаем функциональные тесты
    if settings_data['structure'] and settings_data['functional']:
        pytest.main(generate_run_command(allure_log=settings_data['allure_log'],
                                         allure_dir=allure_dir,
                                         flow=settings_data['flow'],
                                         api_url=settings_data['api_url'],
                                         test_run_id=_test_run_id,
                                         debug=settings_data['debug']))

    # Запускаем тесты покрытия
    if settings_data['structure'] and settings_data['coating']:
        pytest.main(generate_run_command(allure_log=settings_data['allure_log'],
                                         allure_dir=allure_dir,
                                         flow=settings_data['flow'],
                                         api_url=settings_data['api_url'],
                                         test_run_id=_test_run_id,
                                         debug=settings_data['debug'],
                                         test_name='test_coating'))

    # Генерируем ENVIRONMENT для Allure отчета
    if settings_data['allure_log'] and allure_dir:
        with open(os.path.join(BASE_DIR, allure_dir, 'environment.properties'), 'w', encoding="UTF8") as fp:
            fp.write("config {0}\n".format(options.config))
            fp.write("base_url {0}\n".format(settings_data['api_url']))
            fp.write("flow {0}\n".format(settings_data['flow']))
            fp.write("debug {0}\n".format(settings_data['debug']))
            fp.write("swagger_file = {0}\n".format(settings_data['swagger_url']))

    # Генерируем пул кастомных ошибок
    with open(os.path.join(BASE_DIR, allure_dir, 'categories.json'), 'w', encoding='UTF8') as fp:
        custom_errors_structure =\
            [
                {
                    "name": "Ошибки при проверки струтуры",
                    "traceRegex": ".*StructureGetApiException.*",
                },
                {
                    "name": "Ошибки при проверки струтуры",
                    "traceRegex": ".*flex.exceptions.ValidationError.*",
                },
                {
                    "name": "Ошибка вызова",
                    "messageRegex": ".*Возвращен код.*",
                },
                {
                    "name": "Некорректная работа параметра",
                    "messageRegex": ".*Некорректная работа параметра.*",
                },
                {
                    "name": "Непротестированные вызовы",
                    "messageRegex": ".*отсутствует в тестах!.*",
                },
                {
                    "name": "Блокированные тесты",
                    "messageRegex": ".*Блокировано тестом структуры.*",
                },
                {
                    "name": "Блокированные тесты",
                    "traceRegex": ".*TestBlockerException.*",
                },
                {
                    "name": "Ошибки связанные с запросами /order(s)/*",
                    "traceRegex": ".*OrderException.*",
                },
            ]

        fp.write(json.dumps(custom_errors_structure))

    # Генерируем EXECUTORS для Allure отчета
    if options.task_id:
        task_name, build_number = options.task_id.split('-')

        executors_structure = {
            "name": "Jenkins",
            "type": "jenkins",
            "url": "http://jenkins.vsemayki.pro/",
            "buildOrder": build_number,
            "buildName": "{0}#{1}".format(task_name, build_number),
            "buildUrl": "http://jenkins.vsemayki.pro/job/{0}/{1}/".format(task_name, build_number),
            "reportUrl": "http://testserver1.vsemayki.pro/allure-html/{0}".format(_test_run_id),
            "reportName": "AllureReport"
        }

    else:
        executors_structure = {
            "name": "Local",
            "type": "local",
            "url": None,
            "buildOrder": None,
            "buildName": None,
            "buildUrl": None,
            "reportUrl": None,
            "reportName": "AllureReport"
        }

    with open(os.path.join(allure_dir, 'executor.json'), 'w', encoding='UTF-8') as fp:
        fp.write(json.dumps(executors_structure))

    # Генерируем Allure html отчет
    allure_destination_path = allure_html if not options.task_id else os.path.join(allure_html, str(_test_run_id))
    from subprocess import call
    call([ALLURE_BIN_PATH, "generate", allure_dir, "-o", allure_destination_path])

    # Выводим ссылку если запускали из Jenkins
    if options.task_id:
        print('Report href: http://testserver1.vsemayki.pro/allure-html/{0}'.format(_test_run_id))


if __name__ == '__main__':
    main()
