import json
import platform
from typing import Union

import allure
import flex
import os
import pytest
import time

from allure_commons.types import LinkType

from apiClient.core.request import Response
from error_data.errors import ApiFindError
from run import BASE_DIR
from test_data.oauth2.token.variant import UserNoCancelOrder


class BaseAutoTest(object):
    test_run_id = None
    test_case_name = None
    request = None
    api_client = None
    schema = None
    api_url = None
    api_find_error = None

    def check_blocked(self, method, path):
        test_case_id = '{0}|{1}'.format(path, method)
        data = self.load_value(test_case_id)

        if data is not True and data != "NoValue":
            from core.exceptions import TestBlockerException
            raise TestBlockerException('Блокировано тестом структуры {0} {1}'.format(method.upper(), path))

    @pytest.fixture(scope='class', autouse=True)
    def schema(self):
        return flex.load(os.path.join(BASE_DIR, 'tmp', 'test_api.json'))

    @pytest.yield_fixture(autouse=True, scope='function')
    def init_and_teardown(self, request, test_run_id, schema, api_url):
        self.request = request
        self.test_run_id = test_run_id
        self.test_case_name = request.function.__name__
        self.api_url = api_url
        self.schema = schema
        self.api_find_error = ApiFindError()
        yield
        pass

    def wait_for_value(self, name, timeout=60):
        for item in range(0, timeout*10):
            value = self.load_value(name=name)
            if value != 'NoValue':
                return value
            else:
                time.sleep(0.1)

    def save_value(self, name, value):
        self.request.config.cache.set(self._generate_name(name=name.lower()), value)

    def load_value(self, name):
        return self.request.config.cache.get(self._generate_name(name=name.lower()), 'NoValue')

    def _generate_name(self, name):
        return "{0}_{1}".format(self.test_run_id, name)

    @staticmethod
    def log_json_data(log_data: Union[list, dict], log_message=None, allure_step=True, step_name=None):
        log_message = log_message if log_message is not None else "Данные шага"
        step_name = log_message if step_name is None else step_name

        def _log(_log_data, _log_message):
            allure.attach(json.dumps(_log_data), name=_log_message, attachment_type=allure.attachment_type.JSON)

        if allure_step:
            with allure.step(step_name):
                _log(_log_data=log_data, _log_message=log_message)
        else:
            _log(_log_data=log_data, _log_message="Данные шага")
        pass

    @staticmethod
    def log_text(log_message):
        with allure.step(str(log_message)):
            pass

    @staticmethod
    def log_href(log_data, log_message):
        allure.attach(log_data, name=log_message, attachment_type=allure.attachment_type.URI_LIST)

    @staticmethod
    @allure.step('Response headers')
    def _log_headers_as_table(**kwargs):
        pass

    @staticmethod
    @allure.step('Записываем данные запроса')
    def log_request_data(**kwargs):
        pass

    def log_response_data(self, data: Response, log_data=True):
        with allure.step('Записываем данные ответа'):
            allure.attach(data.url, name='Ссылка запроса', attachment_type=allure.attachment_type.URI_LIST)
            self._log_headers_as_table(**data.headers)

            if bool(data.text) and log_data:
                allure.attach(json.dumps(data.json()), name="Данные", attachment_type=allure.attachment_type.JSON)

    def default_structure_test(self, request, access_token=None, **kwargs):
        with allure.step('Выполняем запрос'):
            if access_token:
                response = request(access_token=access_token, **kwargs)
            else:
                response = request(**kwargs)
        self.log_response_data(response)

        with allure.step('Проверяем структуру'):
            response.check_structure()

        return response

    def default_different_filters_not_eq_test(self, request, filter_name, value1, value2, access_token=None, **kwargs):
        allure.dynamic.description("Проверка различия данных при разных значениях фильтров")

        with allure.step('Делаем запросы'):
            if access_token:
                resp1 = request(access_token=access_token, **{filter_name: value1}, **kwargs)
                resp2 = request(access_token=access_token, **{filter_name: value2}, **kwargs)
            else:
                resp1 = request(**{filter_name: value1}, **kwargs)
                resp2 = request(**{filter_name: value2}, **kwargs)

        if resp1.json() == resp2.json():

            self.log_response_data(resp1)
            self.log_response_data(resp2)

            error_message_template = "Одинаковые данные при разных значениях (\"{0}\" и \"{1}\") фильтра {2}"
            pytest.fail(error_message_template.format(value1, value2, filter_name))

    def default_different_filters_eq_test(self, request, filter_name, value1, value2, access_token=None, **kwargs):
        allure.dynamic.description("Проверка совпадения данных при разных значениях фильтров")

        with allure.step('Делаем запросы'):
            if access_token:
                resp1 = request(access_token=access_token, **{filter_name: value1}, **kwargs)
                resp2 = request(access_token=access_token, **{filter_name: value2}, **kwargs)
            else:
                resp1 = request(**{filter_name: value1, **kwargs})
                resp2 = request(**{filter_name: value2, **kwargs})

        if resp1.json() != resp2.json():

            self.log_response_data(resp1)
            self.log_response_data(resp2)

            error_message_template = "Разные данные при разных значениях (\"{0}\" и \"{1}\") фильтра {2}"
            pytest.fail(error_message_template.format(value1, value2, filter_name))

    def get_vmr_access_token(self):
        with allure.step('Получаем из кеша access token для vsemayki'):
            return self.wait_for_value('vmr_access_token')

    def get_user_access_token(self):
        with allure.step('Получаем из кеша access token для юзера'):
            return self.wait_for_value('user_access_token')

    def get_user_no_cancel_order_access_token(self):
        with allure.step('Получаем из кеша access token для юзера {0}' .format(UserNoCancelOrder.username)):
            return self.wait_for_value('user_no_cancel_order_access_token')

    def check_has_error_in_asana(self, test_case_id):
        error_url = self.api_find_error.find_error(request_id=test_case_id)

        if error_url:
            allure.dynamic.link(url=error_url, link_type=LinkType.LINK, name='Ссылка на свзанную таску в Asana')

    def get_test_case_id(self, path, method):
        test_case_id = '{0}_{1}'.format(path.replace("/", ""), method)
        self.log_text(log_message=test_case_id)
        return test_case_id
