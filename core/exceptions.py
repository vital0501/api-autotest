class ApiAutotestException(Exception):
    pass


class TestBlockerException(ApiAutotestException):
    pass


class StructureApiException(ApiAutotestException):
    pass


class RequestException(ApiAutotestException):
    pass


class TestAsanaException(ApiAutotestException):
    pass


class FunctionalException(ApiAutotestException):
    pass


class OrderException(FunctionalException):
    pass
