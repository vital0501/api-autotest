import importlib
import json
import platform

import allure
import os
import pytest
import requests

from apiClient.core.request import Response
from core.base_test import BaseAutoTest
from run import BASE_DIR
from test_data.core.utils import get_all_requests, get_query_id_parameter, path_normalize, get_values, \
    get_structure_object


def generate_test_data():
    result = get_all_requests(path=os.path.join(BASE_DIR, 'tmp', 'test_api.json'))

    for data_pair in result:
        _obj = get_structure_object(path=data_pair[1])

        if _obj is not None:
            values = get_values(structure_obj=_obj, method=data_pair[0])
        else:
            values = {}

        for value_item in values:
            item_by_find = get_query_id_parameter(path=data_pair[1])
            _path = path_normalize(path=data_pair[1], value_item=value_item)
            if item_by_find:
                value_item.pop(item_by_find, None)
            yield data_pair[0], data_pair[1], _path, value_item


@allure.epic('Тест структуры')
@pytest.mark.run(order=2)
class TestAutoApiGet(BaseAutoTest):

    @pytest.mark.parametrize("method, path, real_path, data", generate_test_data())
    def test_structure(self, method, path, real_path, data):
        test_case_id = self.get_test_case_id(path, method)
        self.check_has_error_in_asana(test_case_id=test_case_id)
        self.save_value(name=test_case_id, value=False)

        allure.dynamic.feature(path)
        allure.dynamic.story(method)

        if data is None:
            data = {}

        if method in ['get', 'head']:
            if path in ['/user/profile', '/user/favorites'] or 'user/orders' in path:
                data.update({'access-token': self.get_user_access_token()})
            else:
                data.update({'access-token': self.get_vmr_access_token()})
            _data = {'params': data}
        elif method == 'post':
            _data = {'json': data, 'params': {'access-token': self.get_vmr_access_token()}}
        else:
            _data = {}

        response = Response.from_requests_response(requests.request(method=method.upper(),
                                                                    url=self.api_url + real_path,
                                                                    **_data),
                                                   sw_id='{0}|{1}'.format(path, method), schema=self.schema)

        self.log_response_data(response)

        self.save_value(name=response.get_sw_id(), value=True)

        with allure.step("Проверка структуры"):
            response.check_structure()
