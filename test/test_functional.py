import platform
import allure
import os
import pytest
import requests

from apiClient.core.request import Response
from core.base_test import BaseAutoTest
from core.exceptions import FunctionalException, OrderException
from run import BASE_DIR
from test_data.core.utils import get_all_requests, get_structure_object, get_values, get_query_id_parameter, \
    path_normalize
from test_data.oauth2.token.variant import User, UserNoCancelOrder


def generate_parameter_test_data(parameter):
    _path = os.path.join(BASE_DIR, 'tmp', 'test_api.json')

    result = get_all_requests(path=_path, encoding='UTF8', has_parameter=parameter)

    for data_pair in result:
        _obj = get_structure_object(path=data_pair[1])

        if _obj is not None:
            values = get_values(structure_obj=_obj, method=data_pair[0], data_filter='required_data')
        else:
            values = {}

        for value_item in values:
            item_by_find = get_query_id_parameter(path=data_pair[1])
            _path = path_normalize(path=data_pair[1], value_item=value_item)
            if item_by_find:
                value_item.pop(item_by_find, None)
            yield data_pair[0], data_pair[1], _path, value_item, getattr(_obj(), 'get_collection_json_path')()


@allure.epic('Функциональные тесты')
class TestAutoApiDefaultFilterFunctional(BaseAutoTest):
    def prepare(self, path, method, required_data, real_path):
        allure.dynamic.feature(path)
        allure.dynamic.story(method)

        self.check_blocked(method=method, path=path)

        if required_data is None:
            required_data = {}

        if method in ['get', 'head']:
            if real_path in ['/user/orders']:
                self.log_text('Use user access token')
                required_data.update({'access-token': self.get_user_access_token()})
            else:
                self.log_text('Use vmr access token')
                required_data.update({'access-token': self.get_vmr_access_token()})
        return required_data

    def get_elements_collection_from_response(self, required_data, filter_name, filter_value, path, method, real_path,
                                              json_data_path):
        with allure.step("Делаем запрос с параметром limit = {0}".format(filter_value)):

            required_data[filter_name] = filter_value
            if method in ['post', 'get', 'head']:
                _data = {'params': required_data} if method != 'POST' else {'json': required_data}
            else:
                _data = {}
            response = Response.from_requests_response(requests.request(method=method.upper(),
                                                                        url=self.api_url + real_path,
                                                                        **_data),
                                                       sw_id='{0}|{1}'.format(path, method), schema=self.schema)
            self.log_json_data(log_data=response.json(), log_message='Данные запроса')
        response.check_response_code()

        _tmp = response.json()

        if json_data_path is not None:
            for _item in json_data_path.split('.'):
                try:
                    _item = int(_item)
                except Exception:
                    pass

                with allure.step(_item):
                    _tmp = _tmp[_item]
        return _tmp

    @pytest.mark.parametrize("method, path, real_path, required_data, json_data_path",
                             generate_parameter_test_data(parameter='offset'))
    def test_offset(self, method, path, real_path, required_data, json_data_path):
        test_case_id = self.get_test_case_id(path, method)
        self.check_has_error_in_asana(test_case_id=test_case_id)

        required_data = self.prepare(path=path, method=method, required_data=required_data, real_path=real_path)

        elements = {}

        for value in [0, 1]:
            collection = self.get_elements_collection_from_response(required_data=required_data,
                                                                    filter_name='offset',
                                                                    filter_value=value,
                                                                    path=path,
                                                                    method=method,
                                                                    real_path=real_path,
                                                                    json_data_path=json_data_path)
            elements[value] = collection

        with allure.step("Сравниваем колиство элементов в списке с ожидаемым."):
            if elements[0][1] != elements[1][0]:
                pytest.fail("Некорректная работа параметра 'offset'.\n"
                            "Ожидаем: Первый элемент при параметре = 1 равен второму элементу при параметре = 0.\n")

    @pytest.mark.parametrize("method, path, real_path, required_data, json_data_path",
                             generate_parameter_test_data(parameter='limit'))
    def test_limit(self, method, path, real_path, required_data, json_data_path):
        test_case_id = self.get_test_case_id(path, method)
        self.check_has_error_in_asana(test_case_id=test_case_id)

        required_data = self.prepare(path=path, method=method, required_data=required_data, real_path=real_path)

        for value in [2, 1]:
            collection = self.get_elements_collection_from_response(required_data=required_data,
                                                                    filter_name='limit',
                                                                    filter_value=value,
                                                                    path=path,
                                                                    method=method,
                                                                    real_path=real_path,
                                                                    json_data_path=json_data_path)

            with allure.step("Сравниваем колиство элементов в списке с ожидаемым."):
                if len(collection) != value:
                    pytest.fail("Некорректная работа параметра 'limit'.\n"
                                "Ожидаем: Количество элементов соответствует указанному параметру {0}.\n"
                                "Видим: Количество элементов равно {1}" .format(value, len(collection)))


@allure.epic('Функциональные тесты')
class TestAutoUsersFunctional(BaseAutoTest):
    def test_change_password(self):
        path = '/user/change-password'
        allure.dynamic.feature(path)
        allure.dynamic.story('post')

        test_case_id = self.get_test_case_id(path, 'post')
        self.save_value(name=test_case_id, value=False)
        self.check_has_error_in_asana(test_case_id=test_case_id)

        params = {'access-token': self.get_user_access_token()}
        data = {"rest_access_token": self.get_user_access_token(),
                "password": User.password
                }

        response = Response.from_requests_response(requests.post(url=self.api_url + path, json=data, params=params),
                                                   schema=self.schema)

        self.log_response_data(response)
        response.check_structure()

        with allure.step("Проверка струтктуры"):
            self.save_value(name=test_case_id, value=True)


@allure.epic('Функциональные тесты')
class TestOrderFunctional(BaseAutoTest):
    def test_order_cancel(self):
        path = '/orders/{orderId}/cancel'
        method = 'post'
        test_case_id = self.get_test_case_id(path, 'post')
        self.save_value(name=test_case_id, value=False)

        allure.dynamic.feature(path)
        allure.dynamic.story(method)

        self.check_blocked(method='post', path='/orders')
        from test_data.orders.variant import StructureData as order_json_data
        data = order_json_data().get_post_data()[0]
        data['receiver']['email'] = UserNoCancelOrder.username
        params = {'access-token': self.get_user_no_cancel_order_access_token()}

        response = Response.from_requests_response(requests.post(url=self.api_url + '/orders',
                                                                 json=data,
                                                                 params=params),
                                                   schema=self.schema)

        self.log_json_data(log_data=response.json(), log_message='Данные созданного заказа')

        order_id = response.json()['id']

        response = Response.from_requests_response(requests.post(url=self.api_url + '/orders/{0}/cancel'.format(order_id),
                                                                 json=data,
                                                                 params=params),
                                                   schema=self.schema)

        self.log_json_data(log_data=response.json(), log_message='Данные отмененного заказа')

        if response.json()['status'] != 'Отказ':
            raise OrderException('Не изменился статус заказа')

        self.save_value(name=test_case_id, value=True)

