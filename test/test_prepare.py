import allure
import pytest
import swagger_client
from swagger_client import LoginData

from core.base_test import BaseAutoTest


def get_oauth2_data():
    import test_data.oauth2.token.variant as data
    data = (
        {'name': 'user', 'data': data.User()},
        {'name': 'vmr', 'data': data.VMR()},
        {'name': 'user_no_cancel_order', 'data': data.UserNoCancelOrder()}
    )

    for oauth_type in data:
        yield oauth_type['name'], \
              oauth_type['data'].client_id, \
              oauth_type['data'].client_secret, \
              oauth_type['data'].grant_type, \
              oauth_type['data'].username, \
              oauth_type['data'].password


@allure.epic('Подготовка')
@pytest.mark.debug
class TestAutoApiPost(BaseAutoTest):

    @allure.feature('Prepare login')
    @pytest.mark.debug
    @pytest.mark.parametrize("data_type, client_id, client_secret, grant_type, username, password", get_oauth2_data())
    def test_oauth2_token_post(self, data_type, client_id, client_secret, grant_type, username, password):
        with allure.step('Выполняем запрос'):
            login_data = LoginData(client_id=client_id,
                                   client_secret=client_secret,
                                   grant_type=grant_type,
                                   username=username,
                                   password=password)

            response_data = swagger_client.UserApi().oauth2_token_post(login_data=login_data)

            self.log_text(log_message=response_data.access_token)
            self.save_value(name='{0}_access_token'.format(data_type), value=response_data.access_token)
