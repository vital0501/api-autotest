import platform

import allure
import os
import pytest

from core.base_test import BaseAutoTest
from run import BASE_DIR, TEST_ENC
from test_data.core.utils import get_all_requests, get_description


def generate_coating_request():
    _path = os.path.join(BASE_DIR, 'tmp', 'test_api.json')

    result = get_all_requests(path=_path)

    for method, path in result:
        description = get_description(file_path=_path, method=method, path=path, encoding=TEST_ENC)
        yield method, path, description


@allure.epic("Тест покрытия")
@pytest.mark.debug
class TestAutoApiGet(BaseAutoTest):
    @pytest.mark.parametrize("method, path, description", generate_coating_request())
    def test_coating(self, method, path, description):
        test_case_id = '{0}_{1}'.format(path.replace("/", ""), method)

        allure.dynamic.feature(method)
        allure.dynamic.story(path)
        allure.dynamic.description(description)

        data = self.load_value(test_case_id)

        if data == 'NoValue':
            pytest.fail("Вызов метода '{0} {1}' отсутствует в тестах!".format(method, path))